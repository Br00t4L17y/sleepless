package com.gitlab.Br00t4L17y.sleepless.mixin;

import java.util.List;
import java.util.function.Supplier;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.profiler.Profiler;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.GameRules;
import net.minecraft.world.MutableWorldProperties;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;

@Mixin(ServerWorld.class)
public abstract class SingleSleeperMixin extends World {

    // Auto-generated constructor stub
    protected SingleSleeperMixin(MutableWorldProperties mutableWorldProperties, RegistryKey<World> registryKey,
            RegistryKey<DimensionType> registryKey2, DimensionType dimensionType, Supplier<Profiler> profiler,
            boolean bl, boolean bl2, long l) {
        super(mutableWorldProperties, registryKey, registryKey2, dimensionType, profiler, bl, bl2, l);
    }

    @Shadow
    @Final
    private List<ServerPlayerEntity> players;

    @Shadow
    private boolean allPlayersSleeping;

    @Shadow
    private void wakeSleepingPlayers() {
    }

    @Shadow
    private void resetWeather() {
    }

    @Shadow
    private void method_29199(long l) {
    }

    @Inject(at = @At("TAIL"), method = "tick")
    private void tick(CallbackInfo info) {
        if (this.players.stream().anyMatch((serverPlayerEntity) -> {
            return !serverPlayerEntity.isSpectator() && serverPlayerEntity.isSleepingLongEnough();
        })) {
            this.allPlayersSleeping = false;
            if (this.getGameRules().getBoolean(GameRules.DO_DAYLIGHT_CYCLE)) {
                long l = this.properties.getTimeOfDay() + 24000L;
                this.method_29199(l - l % 24000L);
            }

            this.wakeSleepingPlayers();
            if (this.getGameRules().getBoolean(GameRules.DO_WEATHER_CYCLE)) {
                this.resetWeather();
            }
        }
    }
}